import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap-css-only';
import './animated.css';
import './index.css';
import 'font-awesome/css/font-awesome.min.css';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';

import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';

import { store, history } from './reducers';
import { setCastAvailable } from "./actions/cast";

import LoginScreenContainer from './containers/LoginScreenContainer';
import MoviesScreenContainer from './containers/MoviesScreenContainer';
import MovieDetailScreenContainer from './containers/MovieDetailScreenContainer';
import StreamScreenContainer from './containers/StreamScreenContainer';
import SearchScreenContainer from './containers/SearchScreenContainer';

ReactDOM.render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<Switch>
				<Route exact path="/login" component={LoginScreenContainer} />
				<Route exact path="/" component={MoviesScreenContainer} />
				<Route exact path="/movie/:id" component={MovieDetailScreenContainer} />
				<Route exact path="/stream/:id" component={StreamScreenContainer} />
				<Route exact path="/search/" component={SearchScreenContainer} />
			</Switch>
		</ConnectedRouter>
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();

window['__onGCastApiAvailable'] = (isCastAvailable, reason) => {
	if(isCastAvailable) {
		console.log('Cast is available!');
		return store.dispatch(setCastAvailable(true));
	}

	console.log(reason);
};