import React from 'react';

import { Link } from 'react-router-dom';

function Menu({ back = false, search = false }) {
	return (
		<div className="menu">
			{back ? (
				<Link className="animated fadeIn" to="/">
					<button className="noBackground">
						<i className="fa fa-arrow-left" />
					</button>
				</Link>
			) : (
				<button className="noBackground hidden">
					<i className="fa fa-arrow-left" />
				</button>
			)}
			<h1>FLIX</h1>
			{search ? (
				<Link className="animated fadeIn" to="/search">
					<button className="noBackground">
						<i className="fa fa-search" />
					</button>
				</Link>
			) : (
				<button className="noBackground hidden">
					<i className="fa fa-search" />
				</button>
			)}
		</div>
	);
}

export default Menu;
