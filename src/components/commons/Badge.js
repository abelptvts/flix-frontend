import React from 'react';

function Badge({ title, onClickRemove, id, hasRemoveButton = true, onClick = () => {}, active = true, hasMargin = true}) {
	return (
		<div
			className={`subtitleBadge ${active ? 'active' : ''} ${!hasMargin ? 'noMargin' : ''}`}
			onClick={() => onClick()}
		>
			<span>{title}</span>
			{hasRemoveButton && (
                <button className="noBackground" onClick={() => onClickRemove(id)}>
                    <i className="fa fa-times" />
                </button>
			)}
		</div>
	);
}

export default Badge;
