import React, { Component } from 'react';

class TextDialog extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: ''
		};
	}

	render() {
		const { visible, title, onClose } = this.props;
		return (
			<div className={`dialogContainer ${visible ? '' : 'hiddenDialog'}`}>
				<form
					className="dialog"
					onSubmit={e => {
						e.preventDefault();
						onClose(this.state.value.toLowerCase());
					}}
				>
					<p>{title}</p>
					<input
						value={this.state.value}
						onChange={e => this.setState({ value: e.target.value })}
					/>
					<div className="dialogButtonContainer">
						<button onClick={() => onClose(this.state.value.toLowerCase())}>OK</button>
					</div>
				</form>
			</div>
		);
	}
}

export default TextDialog;
