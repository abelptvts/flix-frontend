import React from 'react';

function Loader() {
	return (
		<div className="animated fadeIn loaderContainer">
			<div className="loader" />
		</div>
	);
}

export default Loader;
