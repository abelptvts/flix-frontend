import React from 'react';
import { Link } from 'react-router-dom';

function Movie({ movie }) {
	return (
		<div className="movieWrapper col-sm-4 col-lg-3">
			<div className="movieCoverWrapper">
				<img
					className="movieCover"
					// src={`http://putovits.sch.bme.hu:5000/api${movie.get(
					// 	'coverImage'
					// )}?token=${window.localStorage.getItem('token')}`}
					src={`https://putovits.tk/api${movie.get(
						'coverImage'
					)}?token=${window.localStorage.getItem('token')}`}
				/>
				<div className="movieOverlay">
					{movie.get('downloaded') ? (
						<Link to={`/stream/${movie.get('id')}`}>
							<i className="play fa fa-play" />
						</Link>
					) : null}
					<Link to={`/movie/${movie.get('id')}`}>
						<button>
							<b className="details">View details</b>
						</button>`
					</Link>
				</div>
			</div>
			<b className="title">{movie.get('title')}</b>
			<div className="titleWrapper">
				<p className="year">{movie.get('year')}</p>
				<div className="ratingWrapper">
					<i className="fa fa-star" />
					<p className="rating">{movie.get('rating')}</p>
				</div>
			</div>
		</div>
	);
}

export default Movie;
