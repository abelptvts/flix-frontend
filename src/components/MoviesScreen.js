import React, { Component } from 'react';

import Movie from './commons/Movie';
import Menu from './commons/Menu';
import Loader from './commons/Loader';
import './MovieScreen.css';

class MoviesScreen extends Component {
	componentDidMount() {
		this.props.getUpcomingMovies();
		this.props.getRecentlyAddedMovies();
	}

	render() {
		return (
			<div className="mainWrapper container">
				<Menu search />

				{this.props.loading ? (
					<Loader />
				) : (
					<div className="animated fadeIn content">
						<div className="recentlyAddedWrapper">
							<h2 className="category">Recently added</h2>
						</div>
						<div className="movieListWrapper row">
							{this.props.downloaded.map((movie, index) => <Movie key={index} movie={movie} />).toList()}
						</div>
						<div className="upcomingWrapper">
							<h2 className="category">Upcoming</h2>
						</div>
						<div className="row">
							{this.props.upcoming.map((movie, index) => <Movie key={index} movie={movie} />).toList()}
						</div>
					</div>
				)}
			</div>
		);
	}
}

export default MoviesScreen;
