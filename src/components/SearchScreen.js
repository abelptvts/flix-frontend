import React, { Component } from 'react';

import InfiniteScroll from 'react-infinite-scroller';

import Movie from './commons/Movie';
import Menu from './commons/Menu';
import Loader from './commons/Loader';
import './MovieScreen.css';

class SearchScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			keyword: '',
			isBrowsing: true,
			hasMore: true
		};
	}

	componentDidMount() {
		this.props.getDownloadedMovies(0);
	}

	componentWillReceiveProps(newProps) {
		console.log(newProps.downloaded.size - this.props.downloaded.size);
	}

	render() {
		return (
			<div className="mainWrapper container">
				<Menu back />

				{this.props.loading ? (
					<Loader />
				) : (
					<div className="animated fadeIn content">
						<form
							className="searchForm"
							onSubmit={e => {
								e.preventDefault();
								this.props.getDownloadedMovies(0, this.state.keyword);
								this.setState({ isBrowsing: this.state.keyword === ''});
							}}
						>
							<input
								className="search"
								placeholder={`Search ${this.props.downloaded.size} movies`}
								value={this.state.keyword}
								onChange={e => this.setState({ keyword: e.target.value })}
							/>
						</form>
						{/* <div className="filtersWrapper">
											<input type="select" placeholder="Category" />
										</div> */}
						<div className="subtitleWrapper">
							<h2 className="category">
								{this.state.isBrowsing ? 'Browse' : 'Search results'}
							</h2>
						</div>
						<InfiniteScroll
							hasMore={this.props.shouldLoadMore}
							initialLoad={false}
							loadMore={page => {
								console.log('loading more!');
								this.props.getDownloadedMovies(page, this.state.keyword)
							}}
							loader={
								<div className="smallLoaderWrapper col-12">
									{/* <div className="loader" /> */}
									Loading...
								</div>
							}
						>
							<div className="movieListWrapper row">
								{this.props.downloaded.map(movie => <Movie movie={movie} />)}
							</div>
						</InfiniteScroll>
					</div>
				)}
			</div>
		);
	}
}

export default SearchScreen;
