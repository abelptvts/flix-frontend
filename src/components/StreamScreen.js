import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import Badge from './commons/Badge';

import './StreamScreen.css';
import './MovieDetailScreen.css'

class StreamScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isCasting: false,
            isPlaying: true,
            progress: 0,
            activeSubId: null,
            loaded: false
        };

        this.castInitialized = false;

        this.onPausedChanged = this.onPausedChanged.bind(this);
        this.onDurationChanged = this.onDurationChanged.bind(this);
        this.onTimeChanged = this.onTimeChanged.bind(this);
        this.onConnectionChanged = this.onConnectionChanged.bind(this);
    }

    componentDidMount() {
        this.props.getMovie(this.props.id);

        if(this.props.isCastAvailable && !this.castInitialized) {
            console.log('init on mount');
            this.initializeCast();
            this.castInitialized = true;
        }
    }

    componentWillReceiveProps(newProps) {
        if(!this.props.isCastAvailable && newProps.isCastAvailable && !this.castInitialized) {
            console.log('init on props');
            this.initializeCast();
            this.castInitialized = true;
        }
    }

    componentWillUnmount() {
        if(typeof this.session !== 'undefined') {
            this.session.endSession(true);
        }

        if(!this.playerController) {
            return;
        }

        const cast = window.cast;

        this.playerController.removeEventListener(
            cast.framework.RemotePlayerEventType.IS_CONNECTED_CHANGED,
            this.onConnectionChanged
        );

        this.playerController.removeEventListener(
            cast.framework.RemotePlayerEventType.CURRENT_TIME_CHANGED,
            this.onTimeChanged
        );

        this.playerController.removeEventListener(
            cast.framework.RemotePlayerEventType.DURATION_CHANGED,
            this.onDurationChanged
        );

        this.playerController.removeEventListener(
            cast.framework.RemotePlayerEventType.IS_PAUSED_CHANGED,
            this.onPausedChanged
        );

        console.log('removed event listeners');
    }

    // cast
    onConnectionChanged() {
        console.log('chromecast connected: ' + this.player.isConnected);
        this.setState({isCasting: this.player.isConnected});

        if (this.player.isConnected) {
            this.playRemote(this.props.movie);
        }
    }

    onTimeChanged() {
        this.setState({
            progress: this.playerController.getSeekPosition(
                this.player.currentTime,
                this.player.duration
            )
        });
    }

    onDurationChanged() {
        this.setState({
            progress: this.playerController.getSeekPosition(
                this.player.currentTime,
                this.player.duration
            )
        });
    }

    onPausedChanged() {
        this.setState({isPlaying: !this.player.isPaused});
    }

    initializeCast() {
        const castButton = document.createElement('button', 'google-cast-button');
        castButton.className = "noBackground";
        castButton.style = 'height: 30px; width: 30px; box-sizing: content-box; padding-left: 0px;';
        const menuDiv = document.getElementById('streamMenu');
        menuDiv.appendChild(castButton);

        const cast = window.cast;
        const chrome = window.chrome;

        cast.framework.CastContext.getInstance().setOptions({
            receiverApplicationId: chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID,
            autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED
        });

        const player = new cast.framework.RemotePlayer();
        const playerController = new cast.framework.RemotePlayerController(player);

        this.player = player;
        this.playerController = playerController;

        this.playerController.addEventListener(
            cast.framework.RemotePlayerEventType.IS_CONNECTED_CHANGED,
            this.onConnectionChanged
        );

        this.playerController.addEventListener(
            cast.framework.RemotePlayerEventType.CURRENT_TIME_CHANGED,
            this.onTimeChanged
        );

        this.playerController.addEventListener(
            cast.framework.RemotePlayerEventType.DURATION_CHANGED,
            this.onDurationChanged
        );

        this.playerController.addEventListener(
            cast.framework.RemotePlayerEventType.IS_PAUSED_CHANGED,
            this.onPausedChanged
        );
    }

    playRemote(movie) {
        const cast = window.cast;
        const chrome = window.chrome;
        const token = window.localStorage.getItem('token');
        const session = cast.framework.CastContext.getInstance().getCurrentSession();

        this.session = session;

        if (session) {
            const mediaInfo = new chrome.cast.media.MediaInfo(
                `https://putovits.tk/stream/${movie.get('id')}?token=${token}`,
                'video/mp4'
            );

            mediaInfo.metadata = new chrome.cast.media.GenericMediaMetadata();
            mediaInfo.metadata.title = movie.get('title');
            mediaInfo.metadata.subtitle = 'watching on FLIX';
            mediaInfo.metadata.images = [{url: `https://putovits.tk/api${movie.get('coverImage')}?token=${token}`}];
            mediaInfo.textTrackStyle = new chrome.cast.media.TextTrackStyle();

            // subtitles here
            mediaInfo.tracks = movie.get('subtitles').map(subtitle => {
                const track = new chrome.cast.media.Track(
                    Number(subtitle.get('id')),
                    chrome.cast.media.TrackType.TEXT
                );
                track.language = 'en-US';
                track.subtype = chrome.cast.media.TextTrackType.SUBTITLES;
                track.trackContentId = `https://putovits.tk/api/subtitles/${subtitle.get('id')}?token=${token}`;
                track.trackContentType = 'text/vtt';
                track.name = subtitle.get('language');
                track.customData = null;
                return track;
            }).toJS();

            const request = new chrome.cast.media.LoadRequest(mediaInfo);
            request.autoPlay = true;
            this.session.loadMedia(request)
                .then(() => {
                    console.log('Loaded!');
                    this.setState({ loaded: true });
                })
                .catch(err => console.log(err));
        }
    }

    seekClick(e) {
        const seekBar = document.getElementById('seekBar');

        if(this.player.canSeek) {
            console.log(e.pageX, seekBar.getBoundingClientRect());
            const percent = 100 * (e.pageX - seekBar.getBoundingClientRect().x) / seekBar.offsetWidth;
            this.player.currentTime = this.playerController.getSeekTime(percent, this.player.duration);
            this.playerController.seek();
            this.setState({progress: percent});
        }
    }

    subtitleClick(sub) {
        const media = this.session.getMediaSession();
        if(media) {
            console.log('editing tracks...');
            const activeTrackIds = [Number(sub.get('id'))];
            const trackInfoRequest = new window.chrome.cast.media.EditTracksInfoRequest(activeTrackIds);

            return media.editTracksInfo(
                trackInfoRequest,
                () => {
                    console.log('track info edited!');
                    this.setState({ activeSubId: sub.get('id') })
                },
                err => {
                    console.log(err);
                }
            )
        }

        console.log('media is undef!');
    }

    render() {
        const token = window.localStorage.getItem('token');

        if (this.props.movie === null) {
            return null;
        }

        const { movie } = this.props;

        return (
            <div className="videoContainer">
                <div id="streamMenu" className="menu">
                    <Link className="animated fadeIn" to="/">
                        <button className="noBackground">
                            <i className="fa fa-arrow-left"/>
                        </button>
                    </Link>
                    <h1>FLIX</h1>
                    {this.props.isCastAvailable === false && <button className="hidden"/>}
                </div>
                <div className="animated fadeIn content" style={{zIndex: 1005}}>
                    {this.state.isCasting ? (
                        <div className="container">
                            <div className="row" style={{flexDirection: 'column', alignItems: 'center'}}>
                                <div className="coverContainer col-10 col-sm-6">
                                    <div className="coverSecondContainer col-12 col-xl-10">
                                        <img
                                            className="cover"
                                            src={`https://putovits.tk/api${movie.get(
                                                'coverImage'
                                            )}?token=${window.localStorage.getItem('token')}`}
                                        />
                                    </div>
                                </div>
                                {this.state.loaded && (
                                    <div className="col-12 col-sm-6 controlsContainer">
                                        <button
                                            className="noBackground playPause"
                                            onClick={() => this.playerController.playOrPause()}
                                        >
                                            <i className={`fa fa-${this.state.isPlaying ? 'pause' : 'play'}`}
                                               style={{marginLeft: this.state.isPlaying ? '' : '3px'}}/>
                                        </button>
                                        <progress
                                            id="seekBar"
                                            className="progress"
                                            max={100}
                                            value={this.state.progress}
                                            title="Video Seek"
                                            onClick={e => this.seekClick(e)}
                                        />
                                    </div>
                                )}
                                {this.state.loaded && (
                                    <div className="subtitlesContainer" style={{justifyContent: 'center'}}>
                                        <div className="subtitleListContainer">
                                            {movie.get('subtitles') && movie.get('subtitles').map((sub, index) => (
                                                <Badge
                                                    hasMargin={index !== movie.get('subtitles').size - 1}
                                                    active={sub.get('id') === this.state.activeSubId}
                                                    onClick={() => this.subtitleClick(sub)}
                                                    hasRemoveButton={false}
                                                    key={sub.get('id')}
                                                    id={sub.get('id')}
                                                    title={sub.get('language')}
                                                />
                                            ))}
                                        </div>
                                    </div>
                                )}
                                {!this.state.loaded && (
                                    <div className="loader"/>
                                )}
                            </div>
                        </div>
                    ) : (
                        <video
                            id="video"
                            className="video"
                            controls
                            crossOrigin="anonymous"
                            src={`https://putovits.tk/stream/${this.props.id}?token=${token}`}
                        >
                            {this.props.movie.get('subtitles') &&
                            this.props.movie
                                .get('subtitles')
                                .map((sub, index) => (
                                    <track
                                        key={index}
                                        label={sub.get('language')}
                                        kind="subtitles"
                                        src={`https://putovits.tk/api/subtitles/${sub.get(
                                            'id'
                                        )}?token=${token}`}
                                    />
                                ))}
                        </video>
                    )}
                </div>
            </div>
        );
    }
}

export default StreamScreen;
