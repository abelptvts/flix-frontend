import React, { Component } from 'react';
import './LoginScreen.css';

import Loader from './commons/Loader';

class LoginScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			password: ''
		};
	}

	componentWillReceiveProps(newProps) {
		console.log(newProps);
	}

	render() {
		return (
			<div className="fullHeight container">
				<div className="centered row">
					{this.props.loading ? (
						<Loader />
					) : (
						<div className="animated fadeIn flex col-12 col-sm-6">
							<h1>FLIX</h1>
							<input
								type="email"
								placeholder="Email address"
								value={this.state.email}
								onChange={e => {
									this.setState({ email: e.target.value });
								}}
							/>
							<input
								type="password"
								placeholder="Password"
								value={this.state.password}
								onChange={e => this.setState({ password: e.target.value })}
							/>
							<div className="buttonWrapper">
								<button
									onClick={() => {
										this.props.login(this.state.email, this.state.password);
									}}
								>
									Login
								</button>
							</div>
							{this.props.error !== null && (
								<span className="error">{this.props.error}</span>
							)}
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default LoginScreen;
