import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Dropzone from 'react-dropzone';

import './MovieDetailScreen.css';

import Menu from './commons/Menu';
import Loader from './commons/Loader';

import Badge from './commons/Badge';
import TextDialog from './commons/TextDialog';

class MovieDetailScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedSubtitle: {},
			showDialog: false,
			showAddSubtitle: false
		};
	}
	componentDidMount() {
		this.props.getMovie(this.props.id);
	}

	render() {
		const movie = this.props.current;

		if (movie === null) {
			return null;
		}

		return (
			<div className="container wrapper">
				<Menu back search />

				{this.props.loading ? (
					<Loader />
				) : (
					<div className="animated fadeIn content">
						<div className="row">
							<div className="coverContainer col-12 col-sm-6">
								<div className="coverSecondContainer col-12 col-xl-10">
									<img
										className="cover"
										// src={`http://putovits.sch.bme.hu:5000/api${movie.get(
										// 	'coverImage'
										// )}?token=${window.localStorage.getItem('token')}`}
										src={`https://putovits.tk/api${movie.get(
											'coverImage'
										)}?token=${window.localStorage.getItem('token')}`}
									/>
								</div>
							</div>
							<div className="movieStuffContainer col-12 col-sm-6">
								<h1 className="titleLarge">{movie.get('title')}</h1>
								<div className="yearContainer">
									<p className="yearLarge">{movie.get('year')}</p>
									<div className="ratingWrapper">
										<i className="starLarge fa fa-star" />
										<p className="ratingLarge">{movie.get('rating')}</p>
									</div>
								</div>
								<div className="playButtonContainer">
									{movie.get('downloaded') ? (
										<Link to={`/stream/${movie.get('id')}`}>
											<button disabled={!movie.get('downloaded')}>
												<i className="fa fa-play" />
												<span className="playText">Stream movie</span>
											</button>
										</Link>
									) : (
										<button disabled={!movie.get('downloaded')}>
											<i className="fa fa-play" />
											<span className="playText">Stream movie</span>
										</button>
									)}
									{!movie.get('downloaded') && (
										<p className="yetToBe">Movie is yet to be downloaded</p>
									)}
								</div>
								{movie.get('downloaded') && (
									<div>
										{movie.get('subtitles').size !== 0 &&
										!this.state.showAddSubtitle ? (
											<div className="subtitlesContainer">
												<div className="subtitleListContainer">
													{movie.get('subtitles').map(sub => (
														<Badge
															key={sub.get('id')}
															id={sub.get('id')}
															title={sub.get('language')}
															onClickRemove={() => {
																this.props.deleteSubtitle(
																	sub.get('id'),
																	this.props.id
																);
															}}
														/>
													))}
												</div>
												{movie.get('subtitles').size < 2 && (
													<button
														className="noBackground"
														onClick={() => {
															this.setState({
																showAddSubtitle: true
															});
														}}
													>
														<i className="fa fa-plus" />
													</button>
												)}
											</div>
										) : (
											<div className="subtitlesContainer">
												<Dropzone
													className="dropzone"
													multiple={false}
													accept=".srt"
													onDrop={files =>
														this.setState({
															selectedSubtitle: files[0]
														})
													}
												>
													<p
														className={`noSubtitles ${this.state
															.selectedSubtitle.name && 'selected'}`}
													>
														{this.state.selectedSubtitle.name ||
															'Click here to add subtitles'}
													</p>
												</Dropzone>

												{this.state.selectedSubtitle.name && (
													<button
														className="animated fadeIn noBackground"
														onClick={() => {
															this.setState({ showDialog: true });
														}}
													>
														<i className="fa fa-check" />
													</button>
												)}

												<TextDialog
													title="Enter subtitle language:"
													visible={this.state.showDialog}
													onClose={language => {
														this.setState({ showDialog: false });
														this.props.addSubtitle(
															this.props.id,
															language,
															this.state.selectedSubtitle
														);
														this.setState({
															selectedSubtitle: {},
															showAddSubtitle: false
														});
													}}
												/>
											</div>
										)}
									</div>
								)}
							</div>
							<div className="descriptionContainer col-12">
								<h2>Description</h2>
								<p className="description">{movie && movie.get('description')}</p>
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}

export default MovieDetailScreen;
