import {
	UPDATE_DOWNLOADED_MOVIES,
	UPDATE_UPCOMING_MOVIES,
	UPDATE_CURRENT_MOVIE,
	RESET_DOWNLOADED_MOVIES
} from '../actions';

import { fromJS } from 'immutable';

const initialState = {
	downloaded: [],
	upcoming: [],
	current: {}
};

export default function movies(state = fromJS(initialState), action) {
	if (action.type === RESET_DOWNLOADED_MOVIES) {
		return state.set('downloaded', fromJS(action.movies));
	}

	if (action.type === UPDATE_DOWNLOADED_MOVIES) {
		return state.set('downloaded', state.get('downloaded').concat(fromJS(action.movies)));
	}

	if (action.type === UPDATE_UPCOMING_MOVIES) {
		return state.set('upcoming', fromJS(action.movies));
	}

	if (action.type === UPDATE_CURRENT_MOVIE) {
		return state.set('current', fromJS(action.movie));
	}

	return state;
}
