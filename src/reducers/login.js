import { UPDATE_LOGIN_ERROR } from '../actions';

import { fromJS } from 'immutable';

const initialState = {
	error: null
};

export default function login(state = fromJS(initialState), action) {
	if (action.type === UPDATE_LOGIN_ERROR) {
		return state.set('error', action.error.message);
	}

	return state;
}
