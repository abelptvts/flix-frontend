import { SET_LOADING } from '../actions';

import { fromJS } from 'immutable';

const initialState = {
	isLoading: false
};

export default function loading(state = fromJS(initialState), action) {
	if (action.type === SET_LOADING && state.get('isLoading') !== action.isLoading) {
		return state.set('isLoading', action.isLoading);
	}

	return state;
}
