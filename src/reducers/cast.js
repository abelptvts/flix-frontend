import { SET_CAST_AVAILABLE } from '../actions';

import { fromJS } from 'immutable';

const initialState = {
    isCastAvailable: false
};

export default function cast(state = fromJS(initialState), action) {
    if (action.type === SET_CAST_AVAILABLE && state.get('isCastAvailable') !== action.isCastAvailable) {
        return state.set('isCastAvailable', action.isCastAvailable);
    }

    return state;
}
