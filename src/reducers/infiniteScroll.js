import { SET_LOAD_MORE } from '../actions';

import { fromJS } from 'immutable';

const initialState = {
	loadMore: false
};

export default function infiniteScroll(state = fromJS(initialState), action) {
	if (action.type === SET_LOAD_MORE) {
		return state.set('loadMore', action.loadMore);
	}

	return state;
}
