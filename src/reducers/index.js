import { combineReducers, compose, createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { routerReducer, routerMiddleware } from 'react-router-redux';
import createBrowserHistory from 'history/es/createBrowserHistory';

import login from './login';
import movies from './movies';
import loading from './loading';
import infiniteScroll from './infiniteScroll';
import cast from './cast';

export const history = createBrowserHistory();
const middleware = routerMiddleware(history);

const appStore = combineReducers({
	login,
	movies,
	loading,
	infiniteScroll,
	cast,
	router: routerReducer
});

// create store
export let store = null;
// eslint-disable-next-line no-undef
store = createStore(
	appStore,
	require('redux-devtools-extension').composeWithDevTools(
		compose(applyMiddleware(thunkMiddleware), applyMiddleware(middleware))
	)
);
// if (__DEV__) {
//
// } else {
// 	// store = createStore(appStore, compose(applyMiddleware(thunkMiddleware, middleware)));
// }
