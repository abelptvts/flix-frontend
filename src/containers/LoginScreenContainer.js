import { connect } from 'react-redux';

import { login } from '../actions/login';

import LoginScreen from '../components/LoginScreen';

function mapStateToProps(state) {
	return {
		error: state.login.get('error'),
		loading: state.loading.get('isLoading')
	};
}

function mapDispatchToProps(dispatch) {
	return {
		login: (email, password) => dispatch(login(email, password))
	};
}

const LoginScreenContainer = connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
export default LoginScreenContainer;
