import { connect } from 'react-redux';

import { getUpcomingMovies, getRecentlyAddedMovies } from '../actions/movies';

import MoviesScreen from '../components/MoviesScreen';

function mapStateToProps(state) {
	return {
		upcoming: state.movies.get('upcoming'),
		downloaded: state.movies.get('downloaded'),
		loading: state.loading.get('isLoading')
	};
}

function mapDispatchToProps(dispatch) {
	return {
		getUpcomingMovies: () => dispatch(getUpcomingMovies()),
		getRecentlyAddedMovies: () => dispatch(getRecentlyAddedMovies())
	};
}

const MoviesScreenContainer = connect(mapStateToProps, mapDispatchToProps)(MoviesScreen);
export default MoviesScreenContainer;
