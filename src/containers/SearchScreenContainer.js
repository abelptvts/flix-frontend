import { connect } from 'react-redux';

import { getDownloadedMovies } from '../actions/movies';

import SearchScreen from '../components/SearchScreen';

function mapStateToProps(state) {
	return {
		downloaded: state.movies.get('downloaded'),
		loading: state.loading.get('isLoading'),
		shouldLoadMore: state.infiniteScroll.get('loadMore')
	};
}

function mapDispatchToProps(dispatch) {
	return {
		getDownloadedMovies: (page, keyword = null) => dispatch(getDownloadedMovies(page, keyword))
	};
}

const SearchScreenContainer = connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
export default SearchScreenContainer;
