import { connect } from 'react-redux';
import { getMovieDetails } from '../actions/movies';

import StreamScreen from '../components/StreamScreen';

function mapStateToProps(state, props) {
	return {
		id: props.match.params.id,
		movie: state.movies.get('current'),
		isCastAvailable: state.cast.get('isCastAvailable')
	};
}

function mapDispatchToProps(dispatch) {
	return {
		getMovie: id => dispatch(getMovieDetails(id))
	};
}

const StreamScreenContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(StreamScreen);
export default StreamScreenContainer;
