import { connect } from 'react-redux';

import { getMovieDetails, addSubtitleTo, removeSubtitle } from '../actions/movies';

import MovieDetailScreen from '../components/MovieDetailScreen';

function mapStateToProps(state, props) {
	return {
		id: props.match.params.id,
		current: state.movies.get('current'),
		loading: state.loading.get('isLoading')
	};
}

function mapDispatchToProps(dispatch) {
	return {
		getMovie: id => dispatch(getMovieDetails(id)),
		addSubtitle: (id, language, subtitleFile) =>
			dispatch(addSubtitleTo(id, language, subtitleFile)),
		deleteSubtitle: (id, movieId) => dispatch(removeSubtitle(id, movieId))
	};
}

const MovieDetailScreenContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(MovieDetailScreen);
export default MovieDetailScreenContainer;
