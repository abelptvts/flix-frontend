import nanoajax from 'nanoajax';

// const baseUrl = 'http://putovits.sch.bme.hu:5000';
// const baseUrl = 'https://putovits.tk:5000';

const baseUrl = 'https://putovits.tk';
// const baseUrl = 'http://192.168.0.17:5000';

function getBody(params) {
	if (params instanceof FormData) {
		return params;
	}

	const body = [];
	Object.keys(params).forEach(key => {
		body.push(`${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`);
	});

	return body.join('&');
}

function apiCall(path, params, isPost) {
	const options = {
		url: baseUrl + path,
		method: 'GET'
	};

	if (isPost) {
		options.method = 'POST';
		options.body = getBody(params);
	}

	options.headers = {};
	const token = window.localStorage.getItem('token');
	if (token !== null) {
		options.headers.authorization = token;
	}

	return new Promise((resolve, reject) => {
		nanoajax.ajax(options, (code, response) => {
			try {
				const res = JSON.parse(response);
				if (code === 401) {
					return resolve({
						code,
						error: res
					});
				}

				if (code === 500) {
					return reject({ code, error: res });
				}

				return resolve({ code, data: res });
			} catch (err) {
				return reject(err);
			}
		});
	});
}

export function apiGet(path) {
	return apiCall(path, null, false);
}

export function apiPost(path, params) {
	return apiCall(path, params, true);
}
