import { SET_CAST_AVAILABLE } from "./index";

export function setCastAvailable(isCastAvailable) {
    return { type: SET_CAST_AVAILABLE, isCastAvailable };
}