import { UPDATE_LOGIN_ERROR } from './';

import { apiPost } from './api';
import { push } from 'react-router-redux';

import { setLoading } from './loading';

function updateLoginError(error) {
	return { type: UPDATE_LOGIN_ERROR, error };
}

export function login(email, password) {
	return async dispatch => {
		dispatch(setLoading(true));

		try {
			const response = await apiPost('/auth/login', { email, password });
			if (response.code === 401) {
				dispatch(setLoading(false));
				return dispatch(updateLoginError(response.error));
			}

			const { token } = response.data;
			window.localStorage.setItem('token', token);

			dispatch(push('/'));
			return dispatch(setLoading(false));
		} catch (err) {
			console.log(err);
		}
	};
}
