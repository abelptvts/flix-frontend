export const UPDATE_LOGIN_ERROR = 'UPDATE_LOGIN_ERROR';
export const UPDATE_DOWNLOADED_MOVIES = 'UPDATE_DOWNLOADED_MOVIES';
export const UPDATE_UPCOMING_MOVIES = 'UPDATE_UPCOMING_MOVIES';
export const UPDATE_CURRENT_MOVIE = 'UPDATE_CURRENT_MOVIE';
export const SET_LOADING = 'SET_LOADING';
export const RESET_DOWNLOADED_MOVIES = 'RESET_DOWNLOADED_MOVIES'; // for browse, infinite scroll
export const SET_LOAD_MORE = 'SET_LOAD_MORE'; // for infinite scroll
export const SET_CAST_AVAILABLE = 'SET_CAST_AVAILABLE';