import { SET_LOADING } from './';

export function setLoading(isLoading) {
	return { type: SET_LOADING, isLoading };
}
