import { apiGet, apiPost } from './api';
import { push } from 'react-router-redux';

import {
	UPDATE_CURRENT_MOVIE,
	UPDATE_DOWNLOADED_MOVIES,
	UPDATE_UPCOMING_MOVIES,
	RESET_DOWNLOADED_MOVIES,
	SET_LOAD_MORE
} from './';

import { setLoading } from './loading';

function resetDownloadedMovies(movies) {
	return { type: RESET_DOWNLOADED_MOVIES, movies };
}

function updateDownloadedMovies(movies) {
	return { type: UPDATE_DOWNLOADED_MOVIES, movies };
}

function setLoadMore(loadMore) {
	return { type: SET_LOAD_MORE, loadMore };
}

function updateUpcomingMovies(movies) {
	return { type: UPDATE_UPCOMING_MOVIES, movies };
}

function updateCurrentMovie(movie) {
	return { type: UPDATE_CURRENT_MOVIE, movie };
}

// THUNK ACTIONS

// private reusable fetch wrapper
async function fetchMovies(downloaded, limit, offset = 0, keyword = null) {
	try {
		const response = await apiGet(
			`/api/movies?downloaded=${downloaded ? 1 : 0}&limit=${limit}&offset=${offset}${
				keyword !== null ? `&keyword=${keyword}` : ''
			}`
		);

		if (response.code === 401) {
			return { isNotAuthenticated: true };
		}

		return response.data;
	} catch (err) {
		// error code 500 is catched here
		console.log(err);
	}

	return { data: [] };
}

const MOVIES_LIMIT = 12;

export function getRecentlyAddedMovies() {
	return async dispatch => {
		dispatch(setLoading(true));

		const response = await fetchMovies(true, MOVIES_LIMIT);

		if (response.isNotAuthenticated) {
			dispatch(setLoading(false));
			return dispatch(push('/login'));
		}

		dispatch(resetDownloadedMovies(response.data));
		return dispatch(setLoading(false));
	};
}

export function getUpcomingMovies() {
	return async dispatch => {
		dispatch(setLoading(true));

		const response = await fetchMovies(false, MOVIES_LIMIT);

		if (response.isNotAuthenticated) {
			dispatch(setLoading(false));
			return dispatch(push('/login'));
		}

		dispatch(updateUpcomingMovies(response.data));
		return dispatch(setLoading(false));
	};
}

export function getDownloadedMovies(page, keyword = null) {
	console.log(`getMovies(${page})`);
	return async dispatch => {
		if (page === 0) {
			dispatch(setLoading(true));
		}

		const response = await fetchMovies(true, MOVIES_LIMIT, page * MOVIES_LIMIT, keyword);

		if (response.isNotAuthenticated) {
			return dispatch(push('/login'));
		}

		if (page === 0) {
			dispatch(setLoadMore(true));
			dispatch(resetDownloadedMovies(response.data));
			return dispatch(setLoading(false));
		}

		if (response.data.length === 0 && page !== 0) {
			dispatch(setLoadMore(false));
		}

		return dispatch(updateDownloadedMovies(response.data));
	};
}

export function getMovieDetails(id) {
	return async dispatch => {
		dispatch(setLoading(true));

		try {
			const response = await apiGet(`/api/movie/${id}`);
			if (response.code === 401) {
				dispatch(setLoading(false));
				return dispatch(push('/login'));
			}

			const movie = response.data.data;
			dispatch(updateCurrentMovie(movie));
			return dispatch(setLoading(false));
		} catch (err) {
			console.log(err);
		}
	};
}

export function addSubtitleTo(id, language, subtitleFile) {
	return async dispatch => {
		try {
			dispatch(setLoading(true));

			const body = new FormData();
			body.set('id', id);
			body.set('language', language);
			body.set('subtitle', subtitleFile);

			const response = await apiPost('/api/subtitles', body);

			if (response.code === 401) {
				dispatch(setLoading(false));
				return dispatch(push('/login'));
			}

			return getMovieDetails(id)(dispatch);
		} catch (err) {
			console.log(err);
			dispatch(setLoading(false));
		}
	};
}

export function removeSubtitle(id, movieId) {
	return async dispatch => {
		try {
			dispatch(setLoading(true));

			const response = await apiGet(`/api/subtitles/remove/${id}`);

			if (response.code === 401) {
				dispatch(setLoading(false));
				return dispatch(push('/login'));
			}

			return getMovieDetails(movieId)(dispatch);
		} catch (err) {
			console.log(err);
			dispatch(setLoading(false));
		}
	};
}
